module gitlab.com/sangha/payment-processors/transactions

go 1.12

require (
	github.com/Rhymond/go-money v0.3.8 // indirect
	github.com/badoux/checkmail v0.0.0-20181210160741-9661bd69e9ad // indirect
	github.com/emicklei/go-restful v2.9.3+incompatible // indirect
	github.com/emicklei/go-restful-swagger12 v0.0.0-20170926063155-7524189396c6 // indirect
	github.com/gosimple/slug v1.7.0 // indirect
	github.com/lib/pq v1.1.1 // indirect
	github.com/muesli/cache2go v0.0.0-20190501130654-46a3a44c1a5f // indirect
	github.com/muesli/smolder v0.0.0-20190507222828-2d53810bc138 // indirect
	github.com/muesli/toktok v0.0.0-20181030220647-94235782aeac // indirect
	github.com/rainycape/unidecode v0.0.0-20150907023854-cb7f23ec59be // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/sirupsen/logrus v1.4.1
	github.com/spf13/cobra v0.0.5 // indirect
	github.com/streadway/amqp v0.0.0-20190404075320-75d898a42a94 // indirect
	github.com/xrash/smetrics v0.0.0-20170218160415-a3153f7040e9 // indirect
	gitlab.com/sangha/mq v0.0.0-20181128100227-b69bcf6541df
	gitlab.com/sangha/sangha v0.0.0-20190826114220-adceadfa7969
	golang.org/x/crypto v0.0.0-20190506204251-e1dfcc566284 // indirect
)
