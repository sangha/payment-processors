package main

import (
	"encoding/json"
	"fmt"

	log "github.com/sirupsen/logrus"

	"gitlab.com/sangha/mq"
	"gitlab.com/sangha/sangha/config"
	"gitlab.com/sangha/sangha/db"
	"gitlab.com/sangha/sangha/logger"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}

func main() {
	configFile := "/conf/transactions.conf"
	logLevelStr := "info"

	logLevel, err := log.ParseLevel(logLevelStr)
	if err != nil {
		log.Fatal(err)
	}
	log.SetLevel(logLevel)

	config.ParseSettings(configFile)
	logger.SetupLogger(config.Settings.Connections.Logger.Protocol,
		config.Settings.Connections.Logger.Address,
		"email-processor")

	log.Infoln("Starting payment transaction processor")

	db.SetupPostgres(config.Settings.Connections.PostgreSQL)
	mq.SetupAMQP(config.Settings.Connections.AMQP)

	context = &db.APIContext{
		Config: *config.Settings,
	}
	context = context.NewAPIContext().(*db.APIContext)

	ch, err := mq.GetAMQPChannel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	msgs, err := ch.Consume(
		"payments", // queue
		"",         // consumer
		false,      // auto-ack
		false,      // exclusive
		false,      // no-local
		false,      // no-wait
		nil,        // args
	)
	failOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			p := mq.Payment{}
			json.Unmarshal(d.Body, &p)
			log.Printf("Received a message: %+v", p)

			if err := processPayment(&p); err != nil {
				panic(err)
			}

			log.Printf("Finished processing transaction!")
			d.Ack(false)
		}
	}()

	log.Println("Waiting for messages. To exit press CTRL+C")
	<-forever
}
