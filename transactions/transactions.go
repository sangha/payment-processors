package main

import (
	"gitlab.com/sangha/mq"
	"gitlab.com/sangha/sangha/config"
	"gitlab.com/sangha/sangha/db"
)

var (
	settings config.Data
	context  *db.APIContext
)

func processPayment(payment *mq.Payment) error {
	p, err := context.LoadPaymentByID(payment.PaymentID)
	if err != nil {
		return err
	}
	return p.Process(context, config.Settings.Processing.DonationCutBudget)
}
