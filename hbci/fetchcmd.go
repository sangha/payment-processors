package main

import (
	"fmt"
	"math/big"

	"github.com/davecgh/go-spew/spew"
	"github.com/spf13/cobra"
	"github.com/umsatz/go-aqbanking"

	"gitlab.com/sangha/sangha/config"
	"gitlab.com/sangha/sangha/db"
)

var (
	fetchCmd = &cobra.Command{
		Use:   "fetch",
		Short: "fetch new payments",
		Long:  `The fetch command retrieves all new incoming payments from the bank via hbci`,
		RunE: func(cmd *cobra.Command, args []string) error {
			return executeFetch()
		},
	}
)

type Pin struct {
	MyBankCode, MyUserID, MyPin string
}

func (p Pin) BankCode() string {
	return p.MyBankCode
}
func (p Pin) UserID() string {
	return p.MyUserID
}
func (p Pin) Pin() string {
	return p.MyPin
}

func executeFetch() error {
	context := (&db.APIContext{
		Config: *config.Settings,
	}).NewAPIContext().(*db.APIContext)

	// Create a client instance
	aq, err := aqbanking.DefaultAQBanking()
	if err != nil {
		panic(err)
	}

	fmt.Printf("using aqbanking %d.%d.%d\n",
		aq.Version.Major,
		aq.Version.Minor,
		aq.Version.Patchlevel,
	)

	user := aqbanking.User{
		ID:          0,
		Name:        config.Settings.PaymentProviders.Hbci.Name,
		UserID:      config.Settings.PaymentProviders.Hbci.UserID,
		CustomerID:  config.Settings.PaymentProviders.Hbci.UserID,
		BankCode:    config.Settings.PaymentProviders.Hbci.BankCode,
		ServerURI:   config.Settings.PaymentProviders.Hbci.URL,
		HbciVersion: 300,
	}

	userCollection, err := aq.Users()
	if err != nil {
		fmt.Printf("unable to list accounts: %v", err)
	}

	for _, u := range userCollection.Users {
		if u.UserID == user.UserID {
			user = u
			break
		}
	}
	fmt.Printf("user: %+v", user)

	if user.ID == 0 {
		if err = aq.AddPinTanUser(&user); err != nil {
			fmt.Println(err)
		}
	}

	p := Pin{
		MyUserID:   config.Settings.PaymentProviders.Hbci.UserID,
		MyBankCode: config.Settings.PaymentProviders.Hbci.BankCode,
		MyPin:      config.Settings.PaymentProviders.Hbci.Pin,
	}
	aq.RegisterPin(p)

	err = user.FetchAccounts(aq)
	if err != nil {
		fmt.Printf("unable to fetch accounts: %v", err)
		// panic(err)
	}

	accountCollection, err := aq.Accounts()
	if err != nil {
		fmt.Printf("unable to list accounts: %v", err)
		// panic(err)
	}

	latestPayment, err := context.LatestPaymentFromSource("hbci")
	if err != nil {
		// panic(err)
	}

	for _, account := range accountCollection {
		fmt.Printf("%v", account)
		transactions, err := aq.AllTransactions(&account)
		if err != nil {
			fmt.Printf("unable to list transactions: %v", err)
		}

		for _, transaction := range transactions {
			spew.Dump(transaction)
			if time.Since(transaction.ValutaDate) <= time.Hour*24 {
				fmt.Println("Skipping, too new")
				continue
			}
			if !transaction.ValutaDate.After(latestPayment.CreatedAt) {
				fmt.Println("Skipping, known")
				continue
			}

			valuei, _ := big.NewFloat(float64(transaction.Total) * 100.0).Int64()
			t := db.Payment{
				BudgetID:      1,
				CreatedAt:     transaction.ValutaDate,
				Amount:        valuei,
				Currency:      "EUR",
				Purpose:       transaction.Purpose,
				RemoteAccount: transaction.RemoteAccountNumber,
				RemoteBankID:  transaction.RemoteBankCode,
				RemoteName:    transaction.RemoteName,
				Source:        "hbci",
			}
			//spew.Dump(t)
			err = t.Save(context)
			if err != nil {
				panic(err)
			}

			/*t := mq.Payment{
				Name: transaction.RemoteName,

				DateTime: transaction.ValutaDate,
				Amount:   valuei,
				Currency: transaction.TotalCurrency,

				Description: transaction.Purpose,

				Source:        "hbci",
				SourcePayerID: transaction.RemoteIBAN,
			}

			pretty.Println(t)
			err = t.Process()
			if err != nil {
				panic(err)
			}*/
		}
	}

	return nil
}

func init() {
	RootCmd.AddCommand(fetchCmd)
}
