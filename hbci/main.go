package main

import (
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"gitlab.com/sangha/mq"
	"gitlab.com/sangha/sangha/config"
	"gitlab.com/sangha/sangha/db"
	"gitlab.com/sangha/sangha/logger"
)

var (
	configFile, logLevelStr string

	// RootCmd is the core command used for cli-arg parsing
	RootCmd = &cobra.Command{
		Use:   "hbci",
		Short: "hbci processor",
		Long: "hbci processor is the hbci processor of the sangha framework\n" +
			"Complete documentation is available at https://gitlab.com/sangha/sangha",
		SilenceErrors: false,
		SilenceUsage:  true,
	}
)

func main() {
	if err := RootCmd.Execute(); err != nil {
		os.Exit(-1)
	}
}

func initConfig() {
	logLevel, err := log.ParseLevel(logLevelStr)
	if err != nil {
		log.Fatal(err)
	}
	log.SetLevel(logLevel)

	config.ParseSettings(configFile)
	logger.SetupLogger(config.Settings.Connections.Logger.Protocol,
		config.Settings.Connections.Logger.Address,
		"hbci-processor")

	log.Infoln("Starting payment hbci processor")

	db.SetupPostgres(config.Settings.Connections.PostgreSQL)
	mq.SetupAMQP(config.Settings.Connections.AMQP)
}

func init() {
	cobra.OnInitialize(initConfig)
	RootCmd.PersistentFlags().StringVarP(&configFile, "config", "c", "config.json", "use this config file (JSON format)")
	RootCmd.PersistentFlags().StringVarP(&logLevelStr, "loglevel", "l", "info", "log level")
}
