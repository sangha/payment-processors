package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/kr/pretty"

	"gitlab.com/sangha/mq"
)

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/notify/", ipnHandler)
	mux.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		http.NotFound(w, req)
	})
	log.Println("PayPal IPN service up and running...")
	log.Fatal(http.ListenAndServe(":9992", mux))
}

func ipnHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, fmt.Sprintf("No route for %v", r.Method), http.StatusNotFound)
		return
	}

	// Switch for production and live
	isProduction := false

	urlSimulator := "https://www.sandbox.paypal.com/cgi-bin/webscr"
	urlLive := "https://www.paypal.com/cgi-bin/webscr"
	paypalURL := urlSimulator
	if isProduction {
		paypalURL = urlLive
	}

	w.WriteHeader(http.StatusOK)

	// Send POST data (IPN message) back as verification
	// Get Content-Type of request to be parroted back to paypal
	contentType := r.Header.Get("Content-Type")
	body, _ := ioutil.ReadAll(r.Body)
	// Prepend POST body with required field
	body = append([]byte("cmd=_notify-validate&"), body...)
	// Make POST request to paypal
	resp, _ := http.Post(paypalURL, contentType, bytes.NewBuffer(body))

	verifyStatus, _ := ioutil.ReadAll(resp.Body)
	if string(verifyStatus) != "VERIFIED" {
		log.Printf("Response: %v", string(verifyStatus))
		log.Println("This indicates that an attempt was made to spoof this interface, or we have a bug.")
		return
	}

	log.Printf("Response: %v\n", string(verifyStatus))
	values, _ := url.ParseQuery(string(body))
	handlePayment(values)
}

func handlePayment(values url.Values) {
	p := mq.Payment{
		Source: "paypal",
	}
	for k, v := range values {
		fmt.Println(k, v)

		switch k {
		case "first_name":
			p.Name += " " + v[0]
		case "last_name":
			p.Name += " " + v[0]
		case "address_street":
			p.Address = []string{
				v[0],
			}
		case "address_name":
		case "address_city":
			p.City = v[0]
		case "address_zip":
			p.ZIP = v[0]
		case "address_country":
			p.Country = v[0]
		case "phone":
			p.Phone = v[0]
		case "payer_email":
			p.Email = v[0]
		case "payment_date":
			p.DateTime = time.Now()
		case "mc_gross":
			amountf, _ := strconv.ParseFloat(v[0], 64)
			p.Amount = int64(amountf * 100.0)
			p.AmountS = v[0]
		case "mc_currency":
			p.Currency = v[0]
		case "custom":
			p.TransactionCode = v[0]
		case "payer_id":
			p.SourcePayerID = v[0]
		}
	}

	pretty.Println(p)
	/*err = p.Process()
	if err != nil {
		panic(err)
	}*/
}
