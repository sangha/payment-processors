package main

import (
	"fmt"
	"math/big"
	"os"

	"github.com/davecgh/go-spew/spew"
	"github.com/kr/pretty"
	"github.com/logpacker/PayPal-Go-SDK"
	"github.com/spf13/cobra"

	"gitlab.com/sangha/mq"
	"gitlab.com/sangha/sangha/config"
)

var (
	fetchCmd = &cobra.Command{
		Use:   "fetch",
		Short: "fetch new payments",
		Long:  `The fetch command retrieves all new incoming payments from PayPal`,
		RunE: func(cmd *cobra.Command, args []string) error {
			return executeFetch()
		},
	}
)

func executeFetch() error {
	// Create a client instance
	c, err := paypalsdk.NewClient(config.Settings.PaymentProviders.PayPal.ClientID,
		config.Settings.PaymentProviders.PayPal.Secret,
		paypalsdk.APIBaseSandBox)
	if err != nil {
		panic(err)
	}

	logf, err := os.Create("./log/paypal.log")
	if err != nil {
		panic(err)
	}
	defer logf.Close()
	c.SetLog(logf)

	_, err = c.GetAccessToken()
	if err != nil {
		panic(err)
	}

	payments, err := c.GetPayments()
	if err != nil {
		panic(err)
	}

	for _, payment := range payments {
		if len(payment.Transactions) == 0 {
			spew.Dump(payment)
			panic("No transactions found for payment!")
		}
		if len(payment.Transactions) > 1 {
			spew.Dump(payment)
			panic("Too many transactions found for payment!")
		}

		value, ab, err := big.ParseFloat(payment.Transactions[0].Amount.Total, 10, 2, big.ToNearestEven)
		if err != nil {
			spew.Dump(payment)
			panic("Can't parse value of this transaction")
		}
		if ab != 10 {
			spew.Dump(payment)
			panic("Actual base of transaction value is not 10")
		}
		valuei, _ := value.Int64()

		// spew.Dump(payment)
		p := mq.Payment{
			Name: fmt.Sprintf("%s %s", payment.Payer.PayerInfo.FirstName, payment.Payer.PayerInfo.LastName),
			Address: []string{
				payment.Payer.PayerInfo.ShippingAddress.RecipientName,
				payment.Payer.PayerInfo.ShippingAddress.Line1,
			},
			City:    payment.Payer.PayerInfo.ShippingAddress.City,
			ZIP:     payment.Payer.PayerInfo.ShippingAddress.PostalCode,
			Country: payment.Payer.PayerInfo.ShippingAddress.CountryCode,

			Phone: payment.Payer.PayerInfo.Phone,
			Email: payment.Payer.PayerInfo.Email,

			DateTime: *payment.Transactions[0].RelatedResources[0].Sale.CreateTime,
			Amount:   valuei * 100, // in smallest currency unit
			AmountS:  payment.Transactions[0].Amount.Total,
			Currency: payment.Transactions[0].Amount.Currency,

			TransactionCode: payment.Transactions[0].Custom,
			Description:     payment.Transactions[0].Description,

			Source:              "paypal",
			SourceID:            payment.ID,
			SourcePayerID:       payment.Payer.PayerInfo.PayerID,
			SourceTransactionID: payment.Transactions[0].RelatedResources[0].Sale.ID,
		}

		/* ac := accounting.Accounting{Symbol: "E", Precision: 2}
		fmt.Println(ac.FormatMoneyBigFloat(p.Value)) */

		pretty.Println(p)
		err = p.Process()
		if err != nil {
			panic(err)
		}
	}

	return nil
}

func init() {
	RootCmd.AddCommand(fetchCmd)
}
