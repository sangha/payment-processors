package payments

import (
	"github.com/emicklei/go-restful"
	"github.com/muesli/smolder"
)

// PaymentResource is the resource responsible for /payments
type PaymentResource struct {
	smolder.Resource
}

var (
	_ smolder.GetIDSupported = &PaymentResource{}
	_ smolder.GetSupported   = &PaymentResource{}
)

// Register this resource with the container to setup all the routes
func (r *PaymentResource) Register(container *restful.Container, config smolder.APIConfig, context smolder.APIContextFactory) {
	r.Name = "PaymentResource"
	r.TypeName = "payment"
	r.Endpoint = "payments"
	r.Doc = "Manage payments"

	r.Config = config
	r.Context = context

	r.Init(container, r)
}

// Returns returns the model that will be returned
func (r *PaymentResource) Returns() interface{} {
	return PaymentResponse{}
}
