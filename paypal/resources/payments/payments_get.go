package payments

import (
	"errors"
	"fmt"
	"math/big"
	"net/http"
	"os"

	"github.com/davecgh/go-spew/spew"
	"github.com/emicklei/go-restful"
	"github.com/kr/pretty"
	"github.com/logpacker/PayPal-Go-SDK"
	"github.com/muesli/smolder"

	"gitlab.com/sangha/mq"
	"gitlab.com/sangha/sangha/db"
)

// GetAuthRequired returns true because all requests need authentication
func (r *PaymentResource) GetAuthRequired() bool {
	return false
}

// GetByIDsAuthRequired returns true because all requests need authentication
func (r *PaymentResource) GetByIDsAuthRequired() bool {
	return false
}

// GetDoc returns the description of this API endpoint
func (r *PaymentResource) GetDoc() string {
	return "retrieve payments"
}

// GetParams returns the parameters supported by this API endpoint
func (r *PaymentResource) GetParams() []*restful.Parameter {
	params := []*restful.Parameter{}
	return params
}

func paypal(context smolder.APIContext, id string) (db.Payment, error) {
	ctx := context.(*db.APIContext)
	p := db.Payment{}

	// Create a client instance
	c, err := paypalsdk.NewClient(ctx.Config.PaymentProviders.PayPal.ClientID,
		ctx.Config.PaymentProviders.PayPal.Secret,
		paypalsdk.APIBaseSandBox)
	if err != nil {
		return p, err
	}
	c.SetLog(os.Stdout)

	_, err = c.GetAccessToken()
	if err != nil {
		return p, err
	}

	payment, err := c.GetPayment(id)
	if err != nil {
		return p, err
	}

	if len(payment.Transactions) == 0 {
		spew.Dump(payment)
		panic("No transactions found for payment!")
	}
	if len(payment.Transactions) > 1 {
		spew.Dump(payment)
		panic("Too many transactions found for payment!")
	}

	value, ab, err := big.ParseFloat(payment.Transactions[0].Amount.Total, 10, 16, big.ToNearestEven)
	if err != nil {
		spew.Dump(payment)
		return p, errors.New("Can't parse value of this transaction")
	}
	if ab != 10 {
		spew.Dump(payment)
		return p, errors.New("Actual base of transaction value is not 10")
	}
	valuei, _ := value.Int64()

	// spew.Dump(payment)
	mp := mq.Payment{
		Name: fmt.Sprintf("%s %s", payment.Payer.PayerInfo.FirstName, payment.Payer.PayerInfo.LastName),
		Address: []string{
			payment.Payer.PayerInfo.ShippingAddress.RecipientName,
			payment.Payer.PayerInfo.ShippingAddress.Line1,
		},
		City:    payment.Payer.PayerInfo.ShippingAddress.City,
		ZIP:     payment.Payer.PayerInfo.ShippingAddress.PostalCode,
		Country: payment.Payer.PayerInfo.ShippingAddress.CountryCode,

		Phone: payment.Payer.PayerInfo.Phone,
		Email: payment.Payer.PayerInfo.Email,

		DateTime: *payment.Transactions[0].RelatedResources[0].Sale.CreateTime,
		Amount:   valuei * 100, // in smallest currency unit
		AmountS:  payment.Transactions[0].Amount.Total,
		Currency: payment.Transactions[0].Amount.Currency,

		TransactionCode: payment.Transactions[0].Custom,
		Description:     payment.Transactions[0].Description,

		Source:              "paypal",
		SourceID:            payment.ID,
		SourcePayerID:       payment.Payer.PayerInfo.PayerID,
		SourceTransactionID: payment.Transactions[0].RelatedResources[0].Sale.ID,
	}
	pretty.Println(mp)

	p.Amount = valuei * 100 // in smallest currency unit
	p.Currency = payment.Transactions[0].Amount.Currency
	p.Code = payment.Transactions[0].Custom
	p.Description = payment.Transactions[0].Description
	p.Source = "paypal"
	p.SourceID = payment.ID
	p.SourceTransactionID = payment.Transactions[0].RelatedResources[0].Sale.ID
	p.CreatedAt = *payment.Transactions[0].RelatedResources[0].Sale.CreateTime

	return p, nil
}

// GetByIDs sends out all items matching a set of IDs
func (r *PaymentResource) GetByIDs(context smolder.APIContext, request *restful.Request, response *restful.Response, ids []string) {
	resp := PaymentResponse{}
	resp.Init(context)

	/*
		    TransactionCode:     "CMKDCFMS",
		    Source:              "paypal",
		    SourceID:            "PAY-21W04903AN1610217LG5ERHA",
		    SourcePayerID:       "CKEWRNHKLJLEN",
			SourceTransactionID: "1N943492GW328524E",
	*/

	for _, id := range ids {
		fmt.Println("ID", id)
		payment, err := paypal(context, id)
		if err != nil {
			smolder.ErrorResponseHandler(request, response, smolder.NewErrorResponse(
				http.StatusBadRequest,
				false,
				"Unknown payment ID",
				"PaymentResource POST"))
			return
		}

		resp.AddPayment(&payment)
	}

	resp.Send(response)
}

// Get sends out items matching the query parameters
func (r *PaymentResource) Get(context smolder.APIContext, request *restful.Request, response *restful.Response, params map[string][]string) {
	resp := PaymentResponse{}
	resp.Init(context)
	resp.Send(response)
}
