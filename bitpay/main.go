package main

import (
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"gitlab.com/sangha/mq"
	"gitlab.com/sangha/sangha/config"
	"gitlab.com/sangha/sangha/db"
	"gitlab.com/sangha/sangha/logger"
)

var (
	// RootCmd is the core command used for cli-arg parsing
	RootCmd = &cobra.Command{
		Use:   "bitpay",
		Short: "bitpay processor",
		Long: "bitpay processor is the bitpay processor of the sangha framework\n" +
			"Complete documentation is available at https://gitlab.com/sangha/sangha",
		SilenceErrors: false,
		SilenceUsage:  true,
	}
)

func main() {
	var configFile, logLevelStr string
	RootCmd.PersistentFlags().StringVarP(&configFile, "config", "c", "config.json", "use this config file (JSON format)")
	RootCmd.PersistentFlags().StringVarP(&logLevelStr, "loglevel", "l", "info", "log level")

	logLevel, err := log.ParseLevel(logLevelStr)
	if err != nil {
		log.Fatal(err)
	}
	log.SetLevel(logLevel)

	config.ParseSettings(configFile)
	logger.SetupLogger(config.Settings.Connections.Logger.Protocol,
		config.Settings.Connections.Logger.Address,
		"bitpay-processor")

	log.Infoln("Starting payment bitpay processor")

	db.SetupPostgres(config.Settings.Connections.PostgreSQL)
	mq.SetupAMQP(config.Settings.Connections.AMQP)

	if err := RootCmd.Execute(); err != nil {
		os.Exit(-1)
	}
}
