package main

import (
	"fmt"

	bitpay "github.com/bitpay/bitpay-go/client"
	ku "github.com/bitpay/bitpay-go/key_utils"
	"gitlab.com/sangha/sangha/config"

	"github.com/spf13/cobra"
)

var (
	pairCmd = &cobra.Command{
		Use:   "pair",
		Short: "pair client",
		Long:  `The pair command pairs the client keys with the bitpay api`,
		RunE: func(cmd *cobra.Command, args []string) error {
			return executePairing()
		},
	}
)

func executePairing() error {
	bitpayPem := ku.GeneratePem()
	bitpayID := ku.GenerateSinFromPem(bitpayPem)

	client := bitpay.Client{
		Pem:      bitpayPem,
		ApiUri:   config.Settings.PaymentProviders.Bitpay.URL,
		ClientId: bitpayID,
	}

	token, err := client.PairWithFacade("merchant")
	if err != nil {
		panic(err)
	}

	fmt.Printf("Your Client PEM, store safely:\n%s", bitpayPem)

	fmt.Printf(
		"Please approve %s/dashboard/merchant/api-tokens with pairing code %s",
		config.Settings.PaymentProviders.Bitpay.URL,
		token.PairingCode)

	return nil
}

func init() {
	RootCmd.AddCommand(pairCmd)
}
