package main

import (
	"fmt"

	bitpay "github.com/bitpay/bitpay-go/client"
	ku "github.com/bitpay/bitpay-go/key_utils"
	"github.com/spf13/cobra"

	"gitlab.com/sangha/sangha/config"
)

var (
	fetchCmd = &cobra.Command{
		Use:   "fetch",
		Short: "fetch new payments",
		Long:  `The fetch command retrieves all new incoming payments from bitpay`,
		RunE: func(cmd *cobra.Command, args []string) error {
			return executeFetch()
		},
	}
)

func executeFetch() error {
	client := bitpay.Client{
		Pem:      config.Settings.PaymentProviders.Bitpay.Pem,
		ClientId: ku.GenerateSinFromPem(config.Settings.PaymentProviders.Bitpay.Pem),
		ApiUri:   config.Settings.PaymentProviders.Bitpay.URL,
	}

	invoice, err := client.GetInvoice("invoice_id")
	if err != nil {
		panic(err)
	}

	fmt.Printf("your invoice %+v", invoice)

	return nil
}

func init() {
	RootCmd.AddCommand(fetchCmd)
}
