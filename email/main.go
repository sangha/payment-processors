package main

import (
	"encoding/json"
	"fmt"

	log "github.com/sirupsen/logrus"
	"github.com/streadway/amqp"

	"gitlab.com/sangha/mq"
	"gitlab.com/sangha/sangha/config"
	"gitlab.com/sangha/sangha/logger"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}

func main() {
	configFile := "config.json"
	logLevelStr := "info"

	logLevel, err := log.ParseLevel(logLevelStr)
	if err != nil {
		log.Fatal(err)
	}
	log.SetLevel(logLevel)

	config.ParseSettings(configFile)
	logger.SetupLogger(config.Settings.Connections.Logger.Protocol,
		config.Settings.Connections.Logger.Address,
		"email-processor")
	SetupEmailTemplates(*config.Settings)

	log.Infoln("Starting payment email processor")

	conn, err := amqp.Dial("amqp://quitty:quitty@10.0.3.135:5672/rabbit")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	msgs, err := ch.Consume(
		"sandbox.payments.email", // queue
		"",                       // consumer
		false,                    // auto-ack
		false,                    // exclusive
		false,                    // no-local
		false,                    // no-wait
		nil,                      // args
	)
	failOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			p := mq.Payment{}
			json.Unmarshal(d.Body, &p)
			log.Printf("Received a message: %+v", p)

			if err := SendPaymentConfirmation(&p); err != nil {
				log.Printf("Failed sending email: %v\n", err)
				continue
			}

			log.Printf("Sent email for payment!\n")
			d.Ack(false)
		}
	}()

	log.Println("Waiting for messages. To exit press CTRL+C")
	<-forever
}
