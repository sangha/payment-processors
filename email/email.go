package main

import (
	"bytes"
	"io"
	"text/template"

	"gitlab.com/sangha/mq"
	"gitlab.com/sangha/sangha/config"
	"gitlab.com/sangha/sangha/db"

	"github.com/muesli/gomail"
)

var (
	templates = make(map[string]config.EmailTemplate)
	settings  config.Data
)

// TemplateHelper combines multiple db-structs to make them become
// accessible from the template
type TemplateHelper struct {
	User    *db.User
	Payment *mq.Payment
	BaseURL string
}

// SetupEmailTemplates compiles the email templates
func SetupEmailTemplates(c config.Data) {
	settings = c
	templates["payment_confirmation"] = config.EmailTemplate{
		Subject: c.EmailTemplates.PaymentConfirmation.Subject,
		Text:    c.EmailTemplates.PaymentConfirmation.Text,
		HTML:    c.EmailTemplates.PaymentConfirmation.HTML,
	}
}

func SendPaymentConfirmation(payment *mq.Payment) error {
	tmpl := templates["payment_confirmation"]

	th := TemplateHelper{
		Payment: payment,
		BaseURL: settings.Web.BaseURL,
	}

	m := gomail.NewMessage()
	m.SetHeader("From", settings.Connections.Email.ReplyTo)
	m.SetHeader("To", settings.Connections.Email.AdminEmail)

	var buf bytes.Buffer
	t := template.Must(template.New("payment_confirmation_subject").Parse(tmpl.Subject))
	t.Execute(&buf, th)
	m.SetHeader("Subject", buf.String())

	m.AddAlternativeWriter("text/plain", func(w io.Writer) error {
		t := template.Must(template.New("payment_confirmation_text").Parse(tmpl.Text))
		return t.Execute(w, th)
	})
	m.AddAlternativeWriter("text/html", func(w io.Writer) error {
		t := template.Must(template.New("payment_confirmation_html").Parse(tmpl.HTML))
		return t.Execute(w, th)
	})

	d := gomail.NewDialer(settings.Connections.Email.SMTP.Server, settings.Connections.Email.SMTP.Port,
		settings.Connections.Email.SMTP.User, settings.Connections.Email.SMTP.Password)

	return d.DialAndSend(m)
}
